package service

import (
	"context"
	"fmt"
	"github.com/apache/thrift/lib/go/thrift"
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/common_utils/constant/demo_constant"
	"gitlab.com/bishe-projects/demo_commodity_service/src/domain/entity"
	"gitlab.com/bishe-projects/demo_commodity_service/src/domain/service"
	"gitlab.com/bishe-projects/demo_commodity_service/src/infrastructure/biz_error"
	"gitlab.com/bishe-projects/demo_commodity_service/src/infrastructure/loaders"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/demo_commodity"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/demo_user"
	"strconv"
)

var CommodityApp = new(Commodity)

type Commodity struct{}

func (a *Commodity) GetByID(id int64) (*entity.Commodity, *business_error.BusinessError) {
	return service.CommodityDomain.GetByID(id)
}

func (a *Commodity) GetList() ([]*entity.Commodity, *business_error.BusinessError) {
	return service.CommodityDomain.GetList()
}

func (a *Commodity) GetListByIds(ids []int64) ([]*entity.Commodity, *business_error.BusinessError) {
	return service.CommodityDomain.GetListByIDs(ids)
}

func (a *Commodity) GetRankList(ctx context.Context) ([]*entity.Commodity, *business_error.BusinessError) {
	// 1: get rank list
	getRankListLoader := loaders.NewGetRankListLoader(ctx, demo_constant.CommodityRankListKey, demo_constant.CommodityRankListName, 0, 100)
	if loaderErr := getRankListLoader.Load(); loaderErr != nil {
		return nil, biz_error.GetRankListErr
	}
	if getRankListLoader.RankList == nil || getRankListLoader.RankList.RankItems == nil {
		klog.CtxErrorf(ctx, "[CommodityApp] get rank list failed: rankList or rankItems nil")
		return nil, biz_error.GetRankListErr
	}
	rankItems := getRankListLoader.RankList.RankItems
	rankIds := make([]int64, 0, len(rankItems))
	for _, item := range rankItems {
		commodityId, parseErr := strconv.ParseInt(item.Member, 10, 64)
		if parseErr != nil {
			klog.CtxErrorf(ctx, "[CommodityApp] parse commodityId failed: err=%s", parseErr)
			return nil, biz_error.GetRankListErr
		}
		rankIds = append(rankIds, commodityId)
	}
	// 2: get commodity list by ids
	commodityList, err := service.CommodityDomain.GetListByIDs(rankIds)
	if err != nil {
		return nil, err
	}
	// 3: assemble data
	commodityMap := make(map[int64]*entity.Commodity)
	for _, commodity := range commodityList {
		commodityMap[commodity.ID] = commodity
	}
	commodityRankList := make([]*entity.Commodity, 0, len(rankIds))
	for _, commodityId := range rankIds {
		commodityRankList = append(commodityRankList, commodityMap[commodityId])
	}
	return commodityRankList, nil
}

func (a *Commodity) HasFavorite(ctx context.Context, uid, commodityId int64) (bool, *business_error.BusinessError) {
	getInteractionListLoader := loaders.NewGetInteractionListLoader(ctx, demo_constant.BusinessId, demo_constant.InteractionUserFavoriteCommodityActionId, demo_constant.InteractionUserEntityId, demo_constant.InteractionCommodityEntityId, thrift.Int64Ptr(uid), thrift.Int64Ptr(commodityId))
	if loaderErr := getInteractionListLoader.Load(); loaderErr != nil {
		return false, biz_error.GetInteractionListErr
	}
	if getInteractionListLoader.Interactions == nil {
		klog.CtxErrorf(ctx, "[CommodityApp] get interaction list nil")
		return false, biz_error.GetInteractionListErr
	}
	return len(getInteractionListLoader.Interactions) > 0, nil
}

func (a *Commodity) Favorite(ctx context.Context, uid, commodityId int64) *business_error.BusinessError {
	_, err := service.CommodityDomain.GetByID(commodityId)
	if err != nil {
		return err
	}
	createInteractionLoader := loaders.NewCreateInteractionLoader(ctx, demo_constant.BusinessId, demo_constant.InteractionUserFavoriteCommodityActionId, demo_constant.InteractionUserEntityId, uid, demo_constant.InteractionCommodityEntityId, commodityId)
	if loaderErr := createInteractionLoader.Load(); loaderErr != nil {
		return biz_error.CreateInteractionErr
	}
	return nil
}

func (a *Commodity) FavoriteCancel(ctx context.Context, uid, commodityId int64) *business_error.BusinessError {
	_, err := service.CommodityDomain.GetByID(commodityId)
	if err != nil {
		return err
	}
	removeInteractionLoader := loaders.NewRemoveInteractionLoader(ctx, demo_constant.BusinessId, demo_constant.InteractionUserFavoriteCommodityActionId, demo_constant.InteractionUserEntityId, uid, demo_constant.InteractionCommodityEntityId, commodityId)
	if loaderErr := removeInteractionLoader.Load(); loaderErr != nil {
		return biz_error.RemoveInteractionErr
	}
	return nil
}

func (a *Commodity) GetFavoriteList(ctx context.Context, uid int64) ([]*entity.Commodity, *business_error.BusinessError) {
	getInteractionListLoader := loaders.NewGetInteractionListLoader(ctx, demo_constant.BusinessId, demo_constant.InteractionUserFavoriteCommodityActionId, demo_constant.InteractionUserEntityId, demo_constant.InteractionCommodityEntityId, thrift.Int64Ptr(uid), nil)
	if loaderErr := getInteractionListLoader.Load(); loaderErr != nil {
		return nil, biz_error.GetInteractionListErr
	}
	if getInteractionListLoader.Interactions == nil {
		klog.CtxErrorf(ctx, "[CommodityApp] get interaction list nil")
		return nil, biz_error.GetInteractionListErr
	}
	favorites := getInteractionListLoader.Interactions
	favoriteCommodityIds := make([]int64, 0, len(favorites))
	for _, favorite := range favorites {
		favoriteCommodityIds = append(favoriteCommodityIds, favorite.AcceptorId)
	}
	return service.CommodityDomain.GetListByIDs(favoriteCommodityIds)
}

func (a *Commodity) GetCommentList(ctx context.Context, commodityId int64) ([]*demo_commodity.UserComment, *business_error.BusinessError) {
	entityCommentListLoader := loaders.NewEntityCommentListLoader(ctx, demo_constant.BusinessId, demo_constant.CommentCommodityEntityId, commodityId, 0, nil, nil)
	if loaderErr := entityCommentListLoader.Load(); loaderErr != nil {
		return nil, biz_error.EntityCommentListErr
	}
	commentList := entityCommentListLoader.CommentList
	userIdSet := make(map[int64]bool)
	for _, comment := range commentList {
		userIdSet[comment.Uid] = true
	}
	userIds := make([]int64, 0, len(userIdSet))
	for userId := range userIdSet {
		userIds = append(userIds, userId)
	}
	getUserListByIdsLoader := loaders.NewGetUserListByIdsLoader(ctx, userIds)
	if loaderErr := getUserListByIdsLoader.Load(); loaderErr != nil {
		return nil, biz_error.GetUserListByIdsErr
	}
	userMap := make(map[int64]*demo_user.User)
	for _, user := range getUserListByIdsLoader.UserList {
		userMap[user.Uid] = user
	}
	userCommentList := make([]*demo_commodity.UserComment, 0, len(commentList))
	for _, comment := range commentList {
		user := userMap[comment.Uid]
		userCommentList = append(userCommentList, &demo_commodity.UserComment{
			Comment: comment,
			User: &demo_commodity.User{
				Uid:      user.Uid,
				Username: user.Username,
			},
		})
	}
	return userCommentList, nil
}

func (a *Commodity) Buy(ctx context.Context, uid, amount, commodityId int64) *business_error.BusinessError {
	_, err := service.CommodityDomain.GetByID(commodityId)
	if err != nil {
		return err
	}
	order := &entity.Order{
		CommodityId: commodityId,
		Uid:         uid,
		Amount:      amount,
		Status:      demo_constant.OrderStatusBuy,
	}
	err = service.OrderDomain.Create(order)
	if err != nil {
		return err
	}
	updateRankListLoader := loaders.NewUpdateRankListLoader(ctx, demo_constant.CommodityRankListKey, demo_constant.CommodityRankListName, fmt.Sprintf("%d", commodityId), float64(amount))
	if loaderErr := updateRankListLoader.Load(); loaderErr != nil {
		return biz_error.UpdateRankListErr
	}
	return nil
}
