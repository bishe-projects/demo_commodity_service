package service

import (
	"context"
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/common_utils/constant/demo_constant"
	"gitlab.com/bishe-projects/demo_commodity_service/src/domain/entity"
	"gitlab.com/bishe-projects/demo_commodity_service/src/domain/service"
	"gitlab.com/bishe-projects/demo_commodity_service/src/infrastructure/biz_error"
	"gitlab.com/bishe-projects/demo_commodity_service/src/infrastructure/loaders"
	"gitlab.com/bishe-projects/demo_commodity_service/src/interface/assembler"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/demo_commodity"
)

var OrderApp = new(Order)

type Order struct{}

func (a *Order) GetList(uid int64) ([]*demo_commodity.Order, *business_error.BusinessError) {
	orderList, err := service.OrderDomain.GetList(uid)
	if err != nil {
		return nil, err
	}
	commoditySet := make(map[int64]bool)
	for _, order := range orderList {
		commoditySet[order.CommodityId] = true
	}
	commodityIds := make([]int64, 0, len(commoditySet))
	for commodityId := range commoditySet {
		commodityIds = append(commodityIds, commodityId)
	}
	commodityList, err := service.CommodityDomain.GetListByIDs(commodityIds)
	if err != nil {
		return nil, err
	}
	commodityMap := make(map[int64]*entity.Commodity)
	for _, commodity := range commodityList {
		commodityMap[commodity.ID] = commodity
	}
	orderResultList := make([]*demo_commodity.Order, 0, len(orderList))
	for _, order := range orderList {
		orderResultList = append(orderResultList, &demo_commodity.Order{
			Id:         order.ID,
			Commodity:  assembler.ConvertCommodityEntityToCommodity(commodityMap[order.CommodityId]),
			Uid:        order.Uid,
			Amount:     order.Amount,
			CreateTime: order.CreateTime.Format("2006-01-02 03:04:05"),
			Status:     order.Status,
		})
	}
	return orderResultList, nil
}

func (a *Order) Comment(ctx context.Context, orderId, uid int64, content string) *business_error.BusinessError {
	// 1: get order info
	order, err := service.OrderDomain.GetByID(orderId)
	if err != nil {
		return err
	}
	// 2: publish comment
	publishCommentLoader := loaders.NewPublishCommentLoader(ctx, demo_constant.BusinessId, demo_constant.CommentCommodityEntityId, order.CommodityId, uid, 0, content)
	if loaderErr := publishCommentLoader.Load(); loaderErr != nil {
		return biz_error.PublishCommentErr
	}
	// 3: change order status
	err = service.OrderDomain.Update(order, map[string]interface{}{"status": demo_constant.OrderStatusComment})
	return err
}
