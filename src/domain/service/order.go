package service

import (
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/demo_commodity_service/src/domain/entity"
)

var OrderDomain = new(Order)

type Order struct{}

func (d *Order) GetByID(id int64) (*entity.Order, *business_error.BusinessError) {
	order := new(entity.Order)
	err := order.GetByID(id)
	if err == business_error.DataNotFoundErr {
		return nil, nil
	}
	return order, err
}

func (d *Order) GetList(uid int64) ([]*entity.Order, *business_error.BusinessError) {
	orderList := new(entity.OrderList)
	err := orderList.Get(uid)
	return *orderList, err
}

func (d *Order) Create(order *entity.Order) *business_error.BusinessError {
	return order.Create()
}

func (d *Order) Update(order *entity.Order, values map[string]interface{}) *business_error.BusinessError {
	return order.Update(values)
}
