package service

import (
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/demo_commodity_service/src/domain/entity"
	"gitlab.com/bishe-projects/demo_commodity_service/src/infrastructure/biz_error"
)

var CommodityDomain = new(Commodity)

type Commodity struct{}

func (d *Commodity) GetByID(id int64) (*entity.Commodity, *business_error.BusinessError) {
	commodity := new(entity.Commodity)
	err := commodity.GetByID(id)
	if err == business_error.DataNotFoundErr {
		return nil, biz_error.CommodityNotFoundErr
	}
	return commodity, err
}

func (d *Commodity) GetList() ([]*entity.Commodity, *business_error.BusinessError) {
	commodityList := new(entity.CommodityList)
	err := commodityList.Get()
	return *commodityList, err
}

func (d *Commodity) GetListByIDs(ids []int64) ([]*entity.Commodity, *business_error.BusinessError) {
	if len(ids) == 0 {
		return []*entity.Commodity{}, nil
	}
	commodityList := new(entity.CommodityList)
	err := commodityList.GetByIDs(ids)
	return *commodityList, err
}
