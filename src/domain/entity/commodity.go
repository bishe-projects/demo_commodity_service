package entity

import (
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/demo_commodity_service/src/infrastructure/biz_error"
	"gitlab.com/bishe-projects/demo_commodity_service/src/infrastructure/repo"
	"gitlab.com/bishe-projects/demo_commodity_service/src/infrastructure/repo/po"
	"gorm.io/gorm"
)

type Commodity struct {
	ID    int64
	Name  string
	Desc  string
	Price float64
}

func (c *Commodity) GetByID(id int64) *business_error.BusinessError {
	po, err := repo.CommodityRepo.CommodityDao.GetByID(id)
	if err == gorm.ErrRecordNotFound {
		return business_error.DataNotFoundErr
	}
	if err != nil {
		klog.Errorf("[CommodityAggregate] get commodity by id failed: err=%s", err)
		return biz_error.GetCommodityByIDErr
	}
	c.fillFromPO(po)
	return nil
}

type CommodityList []*Commodity

func (c *CommodityList) Get() *business_error.BusinessError {
	poList, err := repo.CommodityRepo.CommodityDao.GetList()
	if err != nil {
		klog.Errorf("[CommodityAggregate] get commodity list failed: err=%s", err)
		return biz_error.GetCommodityListErr
	}
	c.fillFromPOList(poList)
	return nil
}

func (c *CommodityList) GetByIDs(ids []int64) *business_error.BusinessError {
	poList, err := repo.CommodityRepo.CommodityDao.GetListByIDs(ids)
	if err != nil {
		klog.Errorf("[CommodityAggregate] get commodity list by ids failed: err=%s", err)
		return biz_error.GetCommodityListErr
	}
	if len(ids) != len(poList) {
		klog.Errorf("[CommodityAggregate] part of commodities not found: ids=%+v, poList=%+v", ids, poList)
		return biz_error.PartCommodityListNotFoundErr
	}
	c.fillFromPOList(poList)
	return nil
}

// converter
func (c *Commodity) fillFromPO(po *po.Commodity) {
	c.ID = po.ID
	c.Name = po.Name
	c.Desc = po.Desc
	c.Price = po.Price
}

func (c *CommodityList) fillFromPOList(poList []*po.Commodity) {
	*c = make([]*Commodity, 0, len(poList))
	for _, po := range poList {
		commodity := new(Commodity)
		commodity.fillFromPO(po)
		*c = append(*c, commodity)
	}
}
