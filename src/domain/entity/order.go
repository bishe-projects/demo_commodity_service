package entity

import (
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/demo_commodity_service/src/infrastructure/biz_error"
	"gitlab.com/bishe-projects/demo_commodity_service/src/infrastructure/repo"
	"gitlab.com/bishe-projects/demo_commodity_service/src/infrastructure/repo/po"
	"gorm.io/gorm"
	"time"
)

type Order struct {
	ID          int64
	CommodityId int64
	Uid         int64
	Amount      int64
	CreateTime  time.Time
	Status      int32
}

func (o *Order) GetByID(id int64) *business_error.BusinessError {
	po, err := repo.OrderRepo.OrderDao.GetByID(id)
	if err == gorm.ErrRecordNotFound {
		return business_error.DataNotFoundErr
	}
	if err != nil {
		klog.Errorf("[OrderAggregate] get order by id failed: err=%s", err)
		return biz_error.GetOrderByIDErr
	}
	o.fillFromPO(po)
	return nil
}

func (o *Order) Create() *business_error.BusinessError {
	po := o.convertToPO()
	err := repo.OrderRepo.OrderDao.Create(po)
	if err != nil {
		klog.Errorf("[OrderAggregate] create order failed: err=%s", err)
		return biz_error.CreateOrderErr
	}
	return nil
}

func (o *Order) Update(values map[string]interface{}) *business_error.BusinessError {
	po := o.convertToPO()
	err := repo.OrderRepo.OrderDao.Update(po, values)
	if err != nil {
		klog.Errorf("[OrderAggregate] update order failed: err=%s", err)
		return biz_error.UpdateOrderErr
	}
	return nil
}

type OrderList []*Order

func (o *OrderList) Get(uid int64) *business_error.BusinessError {
	poList, err := repo.OrderRepo.OrderDao.GetList(uid)
	if err != nil {
		klog.Errorf("[OrderAggregate] get order list failed: err=%s", err)
		return biz_error.GetOrderListErr
	}
	o.fillFromPOList(poList)
	return nil
}

// converter
func (o *Order) convertToPO() *po.Order {
	return &po.Order{
		ID:          o.ID,
		CommodityId: o.CommodityId,
		Uid:         o.Uid,
		Amount:      o.Amount,
		CreateTime:  o.CreateTime,
		Status:      o.Status,
	}
}

func (o *Order) fillFromPO(po *po.Order) {
	o.ID = po.ID
	o.CommodityId = po.CommodityId
	o.Uid = po.Uid
	o.Amount = po.Amount
	o.CreateTime = po.CreateTime
	o.Status = po.Status
}

func (o *OrderList) fillFromPOList(poList []*po.Order) {
	*o = make([]*Order, 0, len(poList))
	for _, po := range poList {
		order := new(Order)
		order.fillFromPO(po)
		*o = append(*o, order)
	}
}
