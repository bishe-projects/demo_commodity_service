package loaders

import (
	"context"
	"errors"
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/common_utils/loader"
	"gitlab.com/bishe-projects/demo_commodity_service/src/infrastructure/client"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/comment"
)

type PublishCommentLoader struct {
	loader.CommonLoader
	ctx context.Context
	// req
	bizId        int64
	entityTypeId int64
	entityId     int64
	uid          int64
	content      string
	parentId     int64
	// resp
	Comment *comment.Comment
}

func (l *PublishCommentLoader) Load() error {
	req := l.newReq()
	resp, err := client.CommentClient.PublishComment(l.ctx, req)
	if err == nil && resp.BaseResp != nil && resp.BaseResp.Status < 0 {
		err = errors.New(resp.BaseResp.Message)
	}
	if err != nil {
		klog.CtxErrorf(l.ctx, "[PublishCommentLoader] publish comment failed: req=%+v err=%s", req, err)
		l.SetError(err)
		return err
	}
	l.Comment = resp.Comment
	return nil
}

func (l *PublishCommentLoader) newReq() *comment.PublishCommentReq {
	return &comment.PublishCommentReq{
		BizId:        l.bizId,
		EntityTypeId: l.entityTypeId,
		EntityId:     l.entityId,
		Uid:          l.uid,
		Content:      l.content,
		ParentId:     l.parentId,
	}
}

func NewPublishCommentLoader(ctx context.Context, bizId, entityTypeId, entityId, uid, parentId int64, content string) *PublishCommentLoader {
	return &PublishCommentLoader{
		ctx:          ctx,
		bizId:        bizId,
		entityTypeId: entityTypeId,
		entityId:     entityId,
		uid:          uid,
		content:      content,
		parentId:     parentId,
	}
}
