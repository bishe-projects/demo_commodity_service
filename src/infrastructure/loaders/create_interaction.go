package loaders

import (
	"context"
	"errors"
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/common_utils/loader"
	"gitlab.com/bishe-projects/demo_commodity_service/src/infrastructure/client"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/interaction"
)

type CreateInteractionLoader struct {
	loader.CommonLoader
	// req
	ctx               context.Context
	bizId             int64
	actionId          int64
	initiatorEntityId int64
	initiatorId       int64
	acceptorEntityId  int64
	acceptorId        int64
}

func (l *CreateInteractionLoader) Load() error {
	req := l.newReq()
	resp, err := client.InteractionClient.CreateInteraction(l.ctx, req)
	if err == nil && resp.BaseResp != nil && resp.BaseResp.Status < 0 {
		err = errors.New(resp.BaseResp.Message)
	}
	if err != nil {
		klog.CtxErrorf(l.ctx, "[CreateInteractionLoader] create interaction failed: req=%+v err=%s", req, err)
		l.SetError(err)
		return err
	}
	return nil
}

func (l *CreateInteractionLoader) newReq() *interaction.CreateInteractionReq {
	return &interaction.CreateInteractionReq{
		Interaction: &interaction.Interaction{
			BizId:             l.bizId,
			ActionId:          l.actionId,
			InitiatorEntityId: l.initiatorEntityId,
			InitiatorId:       l.initiatorId,
			AcceptorEntityId:  l.acceptorEntityId,
			AcceptorId:        l.acceptorId,
		},
	}
}

func NewCreateInteractionLoader(ctx context.Context, bizId, actionId, initiatorEntityId, initiatorId, acceptorEntityId, acceptorId int64) *CreateInteractionLoader {
	return &CreateInteractionLoader{
		ctx:               ctx,
		bizId:             bizId,
		actionId:          actionId,
		initiatorEntityId: initiatorEntityId,
		initiatorId:       initiatorId,
		acceptorEntityId:  acceptorEntityId,
		acceptorId:        acceptorId,
	}
}
