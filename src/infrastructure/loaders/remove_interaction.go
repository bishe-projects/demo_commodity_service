package loaders

import (
	"context"
	"errors"
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/common_utils/loader"
	"gitlab.com/bishe-projects/demo_commodity_service/src/infrastructure/client"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/interaction"
)

type RemoveInteractionLoader struct {
	loader.CommonLoader
	// req
	ctx               context.Context
	bizId             int64
	actionId          int64
	initiatorEntityId int64
	initiatorId       int64
	acceptorEntityId  int64
	acceptorId        int64
}

func (l *RemoveInteractionLoader) Load() error {
	req := l.newReq()
	resp, err := client.InteractionClient.RemoveInteraction(l.ctx, req)
	if err == nil && resp.BaseResp != nil && resp.BaseResp.Status < 0 {
		err = errors.New(resp.BaseResp.Message)
	}
	if err != nil {
		klog.CtxErrorf(l.ctx, "[RemoveInteractionLoader] remove interaction failed: req=%+v err=%s", req, err)
		l.SetError(err)
		return err
	}
	return nil
}

func (l *RemoveInteractionLoader) newReq() *interaction.RemoveInteractionReq {
	return &interaction.RemoveInteractionReq{
		Interaction: &interaction.Interaction{
			BizId:             l.bizId,
			ActionId:          l.actionId,
			InitiatorEntityId: l.initiatorEntityId,
			InitiatorId:       l.initiatorId,
			AcceptorEntityId:  l.acceptorEntityId,
			AcceptorId:        l.acceptorId,
		},
	}
}

func NewRemoveInteractionLoader(ctx context.Context, bizId, actionId, initiatorEntityId, initiatorId, acceptorEntityId, acceptorId int64) *RemoveInteractionLoader {
	return &RemoveInteractionLoader{
		ctx:               ctx,
		bizId:             bizId,
		actionId:          actionId,
		initiatorEntityId: initiatorEntityId,
		initiatorId:       initiatorId,
		acceptorEntityId:  acceptorEntityId,
		acceptorId:        acceptorId,
	}
}
