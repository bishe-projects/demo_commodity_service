package loaders

import (
	"context"
	"errors"
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/common_utils/constant/demo_constant"
	"gitlab.com/bishe-projects/common_utils/loader"
	"gitlab.com/bishe-projects/demo_commodity_service/src/infrastructure/client"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/rank"
)

type UpdateRankListLoader struct {
	loader.CommonLoader
	ctx context.Context
	// req
	key    string
	name   string
	member string
	delta  float64
	// resp
}

func (l *UpdateRankListLoader) Load() error {
	req := l.newReq()
	resp, err := client.RankClient.UpdateRankList(l.ctx, req)
	if err == nil && resp.BaseResp != nil && resp.BaseResp.Status < 0 {
		err = errors.New(resp.BaseResp.Message)
	}
	if err != nil {
		klog.CtxErrorf(l.ctx, "[PublishCommentLoader] publish comment failed: req=%+v err=%s", req, err)
		l.SetError(err)
		return err
	}
	return nil
}

func (l *UpdateRankListLoader) newReq() *rank.UpdateRankListReq {
	return &rank.UpdateRankListReq{
		Key:        l.key,
		BusinessId: demo_constant.BusinessId,
		Name:       l.name,
		Member:     l.member,
		Delta:      l.delta,
	}
}

func NewUpdateRankListLoader(ctx context.Context, key, name, member string, delta float64) *UpdateRankListLoader {
	return &UpdateRankListLoader{
		ctx:    ctx,
		key:    key,
		name:   name,
		member: member,
		delta:  delta,
	}
}
