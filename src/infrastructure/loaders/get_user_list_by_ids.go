package loaders

import (
	"context"
	"errors"
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/common_utils/loader"
	"gitlab.com/bishe-projects/demo_commodity_service/src/infrastructure/client"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/demo_user"
)

type GetUserListByIdsLoader struct {
	loader.CommonLoader
	ctx context.Context
	// req
	ids []int64
	// resp
	UserList []*demo_user.User
}

func (l *GetUserListByIdsLoader) Load() error {
	req := l.newReq()
	resp, err := client.UserClient.GetUserListByIds(l.ctx, req)
	if err == nil && resp.BaseResp != nil && resp.BaseResp.Status < 0 {
		err = errors.New(resp.BaseResp.Message)
	}
	if err != nil {
		klog.CtxErrorf(l.ctx, "[GetUserListByIdsLoader] get user list by ids failed: req=%+v err=%s", req, err)
		l.SetError(err)
		return err
	}
	l.UserList = resp.UserList
	return nil
}

func (l *GetUserListByIdsLoader) newReq() *demo_user.GetUserListByIdsReq {
	return &demo_user.GetUserListByIdsReq{
		Ids: l.ids,
	}
}

func NewGetUserListByIdsLoader(ctx context.Context, ids []int64) *GetUserListByIdsLoader {
	return &GetUserListByIdsLoader{
		ctx: ctx,
		ids: ids,
	}
}
