package loaders

import (
	"context"
	"errors"
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/common_utils/loader"
	"gitlab.com/bishe-projects/demo_commodity_service/src/infrastructure/client"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/interaction"
)

type GetInteractionListLoader struct {
	loader.CommonLoader
	// req
	ctx               context.Context
	bizId             int64
	actionId          int64
	initiatorEntityId int64
	acceptorEntityId  int64
	initiatorId       *int64
	acceptorId        *int64
	// resp
	Interactions []*interaction.Interaction
}

func (l *GetInteractionListLoader) Load() error {
	req := l.newReq()
	resp, err := client.InteractionClient.GetInteractionList(l.ctx, req)
	if err == nil && resp.BaseResp != nil && resp.BaseResp.Status < 0 {
		err = errors.New(resp.BaseResp.Message)
	}
	if err != nil {
		klog.CtxErrorf(l.ctx, "[GetInteractionListLoader] get interaction list failed: req=%+v err=%s", req, err)
		l.SetError(err)
		return err
	}
	l.Interactions = resp.InteractionList
	return nil
}

func (l *GetInteractionListLoader) newReq() *interaction.GetInteractionListReq {
	return &interaction.GetInteractionListReq{
		BizId:             l.bizId,
		ActionId:          l.actionId,
		InitiatorEntityId: l.initiatorEntityId,
		AcceptorEntityId:  l.acceptorEntityId,
		InitiatorId:       l.initiatorId,
		AcceptorId:        l.acceptorId,
	}
}

func NewGetInteractionListLoader(ctx context.Context, bizId, actionId, initiatorEntityId, acceptorEntityId int64, initiatorId, acceptorId *int64) *GetInteractionListLoader {
	return &GetInteractionListLoader{
		ctx:               ctx,
		bizId:             bizId,
		actionId:          actionId,
		initiatorEntityId: initiatorEntityId,
		acceptorEntityId:  acceptorEntityId,
		initiatorId:       initiatorId,
		acceptorId:        acceptorId,
	}
}
