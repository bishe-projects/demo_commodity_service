package loaders

import (
	"context"
	"errors"
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/common_utils/constant/demo_constant"
	"gitlab.com/bishe-projects/common_utils/loader"
	"gitlab.com/bishe-projects/demo_commodity_service/src/infrastructure/client"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/rank"
)

type GetRankListLoader struct {
	loader.CommonLoader
	// req
	ctx    context.Context
	key    string
	name   string
	start  int64
	length int64
	// resp
	RankList *rank.RankList
}

func (l *GetRankListLoader) Load() error {
	req := l.newReq()
	resp, err := client.RankClient.GetRankList(l.ctx, req)
	if err == nil && resp.BaseResp != nil && resp.BaseResp.Status < 0 {
		err = errors.New(resp.BaseResp.Message)
	}
	if err != nil {
		klog.CtxErrorf(l.ctx, "[GetRankListLoader] get rank list failed: req=%+v err=%s", req, err)
		l.SetError(err)
		return err
	}
	l.RankList = resp.RankList
	return nil
}

func (l *GetRankListLoader) newReq() *rank.GetRankListReq {
	return &rank.GetRankListReq{
		Key:        l.key,
		BusinessId: demo_constant.BusinessId,
		Name:       l.name,
		Start:      l.start,
		Length:     l.length,
	}
}

func NewGetRankListLoader(ctx context.Context, key, name string, start, length int64) *GetRankListLoader {
	return &GetRankListLoader{
		ctx:    ctx,
		key:    key,
		name:   name,
		start:  start,
		length: length,
	}
}
