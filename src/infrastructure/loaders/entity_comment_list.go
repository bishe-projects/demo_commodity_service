package loaders

import (
	"context"
	"errors"
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/common_utils/loader"
	"gitlab.com/bishe-projects/demo_commodity_service/src/infrastructure/client"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/comment"
)

type EntityCommentListLoader struct {
	loader.CommonLoader
	ctx context.Context
	// req
	bizId        int64
	entityTypeId int64
	entityId     int64
	parentId     int64
	pageNum      *int64
	pageSize     *int64
	// resp
	CommentList []*comment.Comment
}

func (l *EntityCommentListLoader) Load() error {
	req := l.newReq()
	resp, err := client.CommentClient.EntityCommentList(l.ctx, req)
	if err == nil && resp.BaseResp != nil && resp.BaseResp.Status < 0 {
		err = errors.New(resp.BaseResp.Message)
	}
	if err != nil {
		klog.CtxErrorf(l.ctx, "[EntityCommentListLoader] get entity comment list failed: req=%+v err=%s", req, err)
		l.SetError(err)
		return err
	}
	l.CommentList = resp.CommentList
	return nil
}

func (l *EntityCommentListLoader) newReq() *comment.EntityCommentListReq {
	return &comment.EntityCommentListReq{
		BizId:        l.bizId,
		EntityTypeId: l.entityTypeId,
		EntityId:     l.entityId,
		ParentId:     l.parentId,
		PageNum:      l.pageNum,
		PageSize:     l.pageSize,
	}
}

func NewEntityCommentListLoader(ctx context.Context, bizId, entityTypeId, entityId, parentId int64, pageNum, pageSize *int64) *EntityCommentListLoader {
	return &EntityCommentListLoader{
		ctx:          ctx,
		bizId:        bizId,
		entityTypeId: entityTypeId,
		entityId:     entityId,
		parentId:     parentId,
		pageNum:      pageNum,
		pageSize:     pageSize,
	}
}
