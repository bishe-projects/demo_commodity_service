package biz_error

import "gitlab.com/bishe-projects/common_utils/business_error"

var (
	GetCommodityByIDErr          = business_error.NewBusinessError("get commodity by id failed", -10001)
	GetCommodityListErr          = business_error.NewBusinessError("get commodity list failed", -10002)
	GetCommodityListByIDsErr     = business_error.NewBusinessError("get commodity list by ids failed", -10003)
	PartCommodityListNotFoundErr = business_error.NewBusinessError("part of commodities not found", -10004)
	GetOrderByIDErr              = business_error.NewBusinessError("get order by id failed", -10005)
	GetOrderListErr              = business_error.NewBusinessError("get order list failed", -10006)
	CreateOrderErr               = business_error.NewBusinessError("create order failed", -10007)
	UpdateOrderErr               = business_error.NewBusinessError("update order failed", -10008)
	CommodityNotFoundErr         = business_error.NewBusinessError("commodity not found", -10009)
)

// loader error
var (
	GetRankListErr        = business_error.NewBusinessError("get rank list failed", -30001)
	CreateInteractionErr  = business_error.NewBusinessError("create interaction failed", -30002)
	RemoveInteractionErr  = business_error.NewBusinessError("remove interaction failed", -30003)
	GetInteractionListErr = business_error.NewBusinessError("get interaction list failed", -30004)
	PublishCommentErr     = business_error.NewBusinessError("publish comment failed", -30005)
	EntityCommentListErr  = business_error.NewBusinessError("get entity comment list failed", -30006)
	GetUserListByIdsErr   = business_error.NewBusinessError("get user list by ids failed", -30007)
	UpdateRankListErr     = business_error.NewBusinessError("update rank list failed", -30008)
)
