package repo

import (
	"gitlab.com/bishe-projects/demo_commodity_service/src/infrastructure/repo/mysql"
	"gitlab.com/bishe-projects/demo_commodity_service/src/infrastructure/repo/po"
)

type OrderDao interface {
	GetByID(id int64) (*po.Order, error)
	GetList(uid int64) ([]*po.Order, error)
	Create(order *po.Order) error
	Update(order *po.Order, values map[string]interface{}) error
}

var OrderRepo = NewOrder(mysql.OrderDao)

type Order struct {
	OrderDao OrderDao
}

func NewOrder(orderDao OrderDao) *Order {
	return &Order{
		OrderDao: orderDao,
	}
}
