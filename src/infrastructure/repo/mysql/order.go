package mysql

import (
	"gitlab.com/bishe-projects/common_utils/error_enum"
	"gitlab.com/bishe-projects/common_utils/mysql"
	"gitlab.com/bishe-projects/demo_commodity_service/src/infrastructure/repo/po"
)

var OrderDao = new(Order)

type Order struct{}

const orderTable = "demo_order"

func (r *Order) GetByID(id int64) (*po.Order, error) {
	var order *po.Order
	result := db.Table(orderTable).First(&order, id)
	return order, result.Error
}

func (r *Order) GetList(uid int64) ([]*po.Order, error) {
	var orderList []*po.Order
	result := db.Table(orderTable).Where("uid = ?", uid).Order("create_time DESC").Scan(&orderList)
	return orderList, result.Error
}

func (r *Order) Create(order *po.Order) error {
	err := db.Table(orderTable).Create(&order).Error
	if err != nil {
		if code := mysql.MySqlErrCode(err); code == mysql.ErrDuplicateEntryCode {
			return error_enum.ErrDuplicateEntry
		}
		return err
	}
	return nil
}

func (r *Order) Update(order *po.Order, values map[string]interface{}) error {
	return db.Table(orderTable).Model(order).Updates(values).Error
}
