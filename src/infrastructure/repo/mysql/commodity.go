package mysql

import "gitlab.com/bishe-projects/demo_commodity_service/src/infrastructure/repo/po"

var CommodityDao = new(Commodity)

type Commodity struct{}

const commodityTable = "demo_commodity"

func (r *Commodity) GetByID(id int64) (*po.Commodity, error) {
	var commodity *po.Commodity
	result := db.Table(commodityTable).First(&commodity, id)
	return commodity, result.Error
}

func (r *Commodity) GetList() ([]*po.Commodity, error) {
	var commodityList []*po.Commodity
	result := db.Table(commodityTable).Scan(&commodityList)
	return commodityList, result.Error
}

func (r *Commodity) GetListByIDs(ids []int64) ([]*po.Commodity, error) {
	var commodityList []*po.Commodity
	result := db.Table(commodityTable).Find(&commodityList, ids)
	return commodityList, result.Error
}
