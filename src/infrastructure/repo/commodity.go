package repo

import (
	"gitlab.com/bishe-projects/demo_commodity_service/src/infrastructure/repo/mysql"
	"gitlab.com/bishe-projects/demo_commodity_service/src/infrastructure/repo/po"
)

type CommodityDao interface {
	GetByID(id int64) (*po.Commodity, error)
	GetList() ([]*po.Commodity, error)
	GetListByIDs(ids []int64) ([]*po.Commodity, error)
}

var CommodityRepo = NewCommodity(mysql.CommodityDao)

type Commodity struct {
	CommodityDao CommodityDao
}

func NewCommodity(commodityDao CommodityDao) *Commodity {
	return &Commodity{
		CommodityDao: commodityDao,
	}
}
