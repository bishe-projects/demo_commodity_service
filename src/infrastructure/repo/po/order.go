package po

import "time"

type Order struct {
	ID          int64
	CommodityId int64
	Uid         int64
	Amount      int64
	CreateTime  time.Time `gorm:"default:null"`
	Status      int32
}
