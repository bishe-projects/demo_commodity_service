package po

type Commodity struct {
	ID    int64
	Name  string
	Desc  string
	Price float64
}
