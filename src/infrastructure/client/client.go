package client

import (
	"github.com/cloudwego/kitex/client"
	"github.com/cloudwego/kitex/transport"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/comment/commentservice"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/demo_user/userservice"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/interaction/interactionservice"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/rank/rankservice"
)

var (
	RankClient        = rankservice.MustNewClient("middle_rank_service", client.WithHostPorts("0.0.0.0:8886"), client.WithTransportProtocol(transport.TTHeaderFramed))
	InteractionClient = interactionservice.MustNewClient("middle_interaction_service", client.WithHostPorts("0.0.0.0:8888"), client.WithTransportProtocol(transport.TTHeaderFramed))
	CommentClient     = commentservice.MustNewClient("middle_comment_service", client.WithHostPorts("0.0.0.0:8887"), client.WithTransportProtocol(transport.TTHeaderFramed))
	UserClient        = userservice.MustNewClient("demo_user_service", client.WithHostPorts("0.0.0.0:8889"), client.WithTransportProtocol(transport.TTHeaderFramed))
)
