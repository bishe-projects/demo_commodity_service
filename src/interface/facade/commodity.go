package facade

import (
	"context"
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/common_utils"
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/demo_commodity_service/src/application/service"
	"gitlab.com/bishe-projects/demo_commodity_service/src/interface/assembler"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/demo_commodity"
)

var CommodityFacade = new(Commodity)

type Commodity struct{}

func (f *Commodity) GetById(ctx context.Context, req *demo_commodity.GetCommodityByIdReq) *demo_commodity.GetCommodityByIdResp {
	resp := demo_commodity.NewGetCommodityByIdResp()
	commodity, err := service.CommodityApp.GetByID(req.CommodityId)
	if err != nil {
		klog.CtxErrorf(ctx, "[CommodityFacade] get commodity list failed: req=%+v err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	resp.Commodity = assembler.ConvertCommodityEntityToCommodity(commodity)
	return resp
}

func (f *Commodity) GetList(ctx context.Context, req *demo_commodity.GetCommodityListReq) *demo_commodity.GetCommodityListResp {
	resp := demo_commodity.NewGetCommodityListResp()
	commodityList, err := service.CommodityApp.GetList()
	if err != nil {
		klog.CtxErrorf(ctx, "[CommodityFacade] get commodity list failed: req=%+v err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	resp.CommodityList = assembler.ConvertCommodityEntityListToCommodityList(commodityList)
	return resp
}

func (f *Commodity) GetListByIds(ctx context.Context, req *demo_commodity.GetCommodityListByIdsReq) *demo_commodity.GetCommodityListByIdsResp {
	resp := demo_commodity.NewGetCommodityListByIdsResp()
	commodityList, err := service.CommodityApp.GetListByIds(req.Ids)
	if err != nil {
		klog.CtxErrorf(ctx, "[CommodityFacade] get commodity list by ids failed: req=%+v err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	resp.CommodityList = assembler.ConvertCommodityEntityListToCommodityList(commodityList)
	return resp
}

func (f *Commodity) GetRankList(ctx context.Context, req *demo_commodity.GetCommodityRankListReq) *demo_commodity.GetCommodityRankListResp {
	resp := demo_commodity.NewGetCommodityRankListResp()
	commodityRankList, err := service.CommodityApp.GetRankList(ctx)
	if err != nil {
		klog.CtxErrorf(ctx, "[CommodityFacade] get commodity rank list failed: req=%+v err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	resp.RankList = assembler.ConvertCommodityEntityListToCommodityList(commodityRankList)
	return resp
}

func (f *Commodity) HasFavorite(ctx context.Context, req *demo_commodity.HasFavoriteCommodityReq) *demo_commodity.HasFavoriteCommodityResp {
	resp := demo_commodity.NewHasFavoriteCommodityResp()
	uid, err := common_utils.GetUidFromCtx(ctx)
	if err != nil {
		klog.CtxErrorf(ctx, "[CommodityFacade] has favorite commodity failed: req=%+v, err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	has, err := service.CommodityApp.HasFavorite(ctx, uid, req.CommodityId)
	if err != nil {
		klog.CtxErrorf(ctx, "[CommodityFacade] has favorite commodity failed: req=%+v, err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	resp.Has = has
	return resp
}

func (f *Commodity) Favorite(ctx context.Context, req *demo_commodity.FavoriteCommodityReq) *demo_commodity.FavoriteCommodityResp {
	resp := demo_commodity.NewFavoriteCommodityResp()
	uid, err := common_utils.GetUidFromCtx(ctx)
	if err != nil {
		klog.CtxErrorf(ctx, "[CommodityFacade] favorite commodity failed: req=%+v, err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	err = service.CommodityApp.Favorite(ctx, uid, req.CommodityId)
	if err != nil {
		klog.CtxErrorf(ctx, "[CommodityFacade] favorite commodity failed: req=%+v, err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	return resp
}

func (f *Commodity) FavoriteCancel(ctx context.Context, req *demo_commodity.FavoriteCancelCommodityReq) *demo_commodity.FavoriteCancelCommodityResp {
	resp := demo_commodity.NewFavoriteCancelCommodityResp()
	uid, err := common_utils.GetUidFromCtx(ctx)
	if err != nil {
		klog.CtxErrorf(ctx, "[CommodityFacade] favorite cancel commodity failed: req=%+v, err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	err = service.CommodityApp.FavoriteCancel(ctx, uid, req.CommodityId)
	if err != nil {
		klog.CtxErrorf(ctx, "[CommodityFacade] favorite cancel commodity failed: req=%+v, err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	return resp
}

func (f *Commodity) GetFavoriteList(ctx context.Context, req *demo_commodity.GetFavoriteListReq) *demo_commodity.GetFavoriteListResp {
	resp := demo_commodity.NewGetFavoriteListResp()
	uid, err := common_utils.GetUidFromCtx(ctx)
	if err != nil {
		klog.CtxErrorf(ctx, "[CommodityFacade] get favorite list failed: req=%+v, err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	favoriteCommodityList, err := service.CommodityApp.GetFavoriteList(ctx, uid)
	if err != nil {
		klog.CtxErrorf(ctx, "[CommodityFacade] get favorite list failed: req=%+v, err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	resp.FavoriteList = assembler.ConvertCommodityEntityListToCommodityList(favoriteCommodityList)
	return resp
}

func (f *Commodity) GetCommentList(ctx context.Context, req *demo_commodity.GetCommodityCommentListReq) *demo_commodity.GetCommodityCommentListResp {
	resp := demo_commodity.NewGetCommodityCommentListResp()
	commentList, err := service.CommodityApp.GetCommentList(ctx, req.CommodityId)
	if err != nil {
		klog.CtxErrorf(ctx, "[CommodityFacade] get comment list failed: req=%+v, err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	resp.CommentList = commentList
	return resp
}

func (f *Commodity) Buy(ctx context.Context, req *demo_commodity.BuyCommodityReq) *demo_commodity.BuyCommodityResp {
	resp := demo_commodity.NewBuyCommodityResp()
	uid, err := common_utils.GetUidFromCtx(ctx)
	if err != nil {
		klog.CtxErrorf(ctx, "[CommodityFacade] buy commodity failed: req=%+v, err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	err = service.CommodityApp.Buy(ctx, uid, req.Amount, req.CommodityId)
	if err != nil {
		klog.CtxErrorf(ctx, "[CommodityFacade] buy commodity failed: req=%+v, err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	return resp
}
