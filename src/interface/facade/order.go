package facade

import (
	"context"
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/common_utils"
	"gitlab.com/bishe-projects/common_utils/business_error"
	"gitlab.com/bishe-projects/demo_commodity_service/src/application/service"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/demo_commodity"
)

var OrderFacade = new(Order)

type Order struct{}

func (f *Order) GetList(ctx context.Context, req *demo_commodity.GetOrderListReq) *demo_commodity.GetOrderListResp {
	resp := demo_commodity.NewGetOrderListResp()
	uid, err := common_utils.GetUidFromCtx(ctx)
	if err != nil {
		klog.CtxErrorf(ctx, "[OrderFacade] get order list failed: req=%+v, err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	orderList, err := service.OrderApp.GetList(uid)
	if err != nil {
		klog.CtxErrorf(ctx, "[OrderFacade] get order list failed: req=%+v, err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	resp.OrderList = orderList
	return resp
}

func (f *Order) Comment(ctx context.Context, req *demo_commodity.CommentOrderReq) *demo_commodity.CommentOrderResp {
	resp := demo_commodity.NewCommentOrderResp()
	uid, err := common_utils.GetUidFromCtx(ctx)
	if err != nil {
		klog.CtxErrorf(ctx, "[OrderFacade] get order list failed: req=%+v, err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	err = service.OrderApp.Comment(ctx, req.OrderId, uid, req.Content)
	if err != nil {
		klog.CtxErrorf(ctx, "[OrderFacade] get order list failed: req=%+v, err=%s", req, err)
		resp.BaseResp = business_error.BaseResp(err)
		return resp
	}
	return resp
}
