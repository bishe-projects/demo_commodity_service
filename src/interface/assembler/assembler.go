package assembler

import (
	"gitlab.com/bishe-projects/demo_commodity_service/src/domain/entity"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/demo_commodity"
)

func ConvertCommodityEntityToCommodity(commodityEntity *entity.Commodity) *demo_commodity.Commodity {
	return &demo_commodity.Commodity{
		Id:    commodityEntity.ID,
		Name:  commodityEntity.Name,
		Desc:  commodityEntity.Desc,
		Price: commodityEntity.Price,
	}
}

func ConvertCommodityEntityListToCommodityList(commodityEntityList []*entity.Commodity) []*demo_commodity.Commodity {
	commodityList := make([]*demo_commodity.Commodity, 0, len(commodityEntityList))
	for _, commodityEntity := range commodityEntityList {
		commodityList = append(commodityList, ConvertCommodityEntityToCommodity(commodityEntity))
	}
	return commodityList
}
