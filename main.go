package main

import (
	"github.com/cloudwego/kitex/server"
	demo_commodity "gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/demo_commodity/commodityservice"
	"log"
	"net"
)

func main() {
	addr, _ := net.ResolveTCPAddr("tcp", "127.0.0.1:8890")
	svr := demo_commodity.NewServer(new(CommodityServiceImpl), server.WithServiceAddr(addr))

	err := svr.Run()

	if err != nil {
		log.Println(err.Error())
	}
}
