package main

import (
	"context"
	"github.com/cloudwego/kitex/pkg/klog"
	"gitlab.com/bishe-projects/demo_commodity_service/src/interface/facade"
	"gitlab.com/bishe-projects/service_kitex_gen/kitex_gen/demo_commodity"
)

// CommodityServiceImpl implements the last service interface defined in the IDL.
type CommodityServiceImpl struct{}

// GetCommodityById implements the CommodityServiceImpl interface.
func (s *CommodityServiceImpl) GetCommodityById(ctx context.Context, req *demo_commodity.GetCommodityByIdReq) (resp *demo_commodity.GetCommodityByIdResp, err error) {
	klog.CtxInfof(ctx, "GetCommodityById req=%+v", req)
	resp = facade.CommodityFacade.GetById(ctx, req)
	klog.CtxInfof(ctx, "GetCommodityById resp=%+v", resp)
	return
}

// GetCommodityList implements the CommodityServiceImpl interface.
func (s *CommodityServiceImpl) GetCommodityList(ctx context.Context, req *demo_commodity.GetCommodityListReq) (resp *demo_commodity.GetCommodityListResp, err error) {
	klog.CtxInfof(ctx, "GetCommodityList req=%+v", req)
	resp = facade.CommodityFacade.GetList(ctx, req)
	klog.CtxInfof(ctx, "GetCommodityList resp=%+v", resp)
	return
}

// GetCommodityListByIds implements the CommodityServiceImpl interface.
func (s *CommodityServiceImpl) GetCommodityListByIds(ctx context.Context, req *demo_commodity.GetCommodityListByIdsReq) (resp *demo_commodity.GetCommodityListByIdsResp, err error) {
	klog.CtxInfof(ctx, "GetCommodityListByIds req=%+v", req)
	resp = facade.CommodityFacade.GetListByIds(ctx, req)
	klog.CtxInfof(ctx, "GetCommodityListByIds resp=%+v", resp)
	return
}

// GetCommodityRankList implements the CommodityServiceImpl interface.
func (s *CommodityServiceImpl) GetCommodityRankList(ctx context.Context, req *demo_commodity.GetCommodityRankListReq) (resp *demo_commodity.GetCommodityRankListResp, err error) {
	klog.CtxInfof(ctx, "GetCommodityRankList req=%+v", req)
	resp = facade.CommodityFacade.GetRankList(ctx, req)
	klog.CtxInfof(ctx, "GetCommodityRankList resp=%+v", resp)
	return
}

// HasFavoriteCommodity implements the CommodityServiceImpl interface.
func (s *CommodityServiceImpl) HasFavoriteCommodity(ctx context.Context, req *demo_commodity.HasFavoriteCommodityReq) (resp *demo_commodity.HasFavoriteCommodityResp, err error) {
	klog.CtxInfof(ctx, "HasFavoriteCommodity req=%+v", req)
	resp = facade.CommodityFacade.HasFavorite(ctx, req)
	klog.CtxInfof(ctx, "HasFavoriteCommodity resp=%+v", resp)
	return
}

// FavoriteCommodity implements the CommodityServiceImpl interface.
func (s *CommodityServiceImpl) FavoriteCommodity(ctx context.Context, req *demo_commodity.FavoriteCommodityReq) (resp *demo_commodity.FavoriteCommodityResp, err error) {
	klog.CtxInfof(ctx, "FavoriteCommodity req=%+v", req)
	resp = facade.CommodityFacade.Favorite(ctx, req)
	klog.CtxInfof(ctx, "FavoriteCommodity resp=%+v", resp)
	return
}

// FavoriteCancelCommodity implements the CommodityServiceImpl interface.
func (s *CommodityServiceImpl) FavoriteCancelCommodity(ctx context.Context, req *demo_commodity.FavoriteCancelCommodityReq) (resp *demo_commodity.FavoriteCancelCommodityResp, err error) {
	klog.CtxInfof(ctx, "FavoriteCancelCommodity req=%+v", req)
	resp = facade.CommodityFacade.FavoriteCancel(ctx, req)
	klog.CtxInfof(ctx, "FavoriteCancelCommodity resp=%+v", resp)
	return
}

// GetFavoriteList implements the CommodityServiceImpl interface.
func (s *CommodityServiceImpl) GetFavoriteList(ctx context.Context, req *demo_commodity.GetFavoriteListReq) (resp *demo_commodity.GetFavoriteListResp, err error) {
	klog.CtxInfof(ctx, "GetFavoriteList req=%+v", req)
	resp = facade.CommodityFacade.GetFavoriteList(ctx, req)
	klog.CtxInfof(ctx, "GetFavoriteList resp=%+v", resp)
	return
}

// GetCommodityCommentList implements the CommodityServiceImpl interface.
func (s *CommodityServiceImpl) GetCommodityCommentList(ctx context.Context, req *demo_commodity.GetCommodityCommentListReq) (resp *demo_commodity.GetCommodityCommentListResp, err error) {
	klog.CtxInfof(ctx, "GetCommodityCommentList req=%+v", req)
	resp = facade.CommodityFacade.GetCommentList(ctx, req)
	klog.CtxInfof(ctx, "GetCommodityCommentList resp=%+v", resp)
	return
}

// BuyCommodity implements the CommodityServiceImpl interface.
func (s *CommodityServiceImpl) BuyCommodity(ctx context.Context, req *demo_commodity.BuyCommodityReq) (resp *demo_commodity.BuyCommodityResp, err error) {
	klog.CtxInfof(ctx, "BuyCommodity req=%+v", req)
	resp = facade.CommodityFacade.Buy(ctx, req)
	klog.CtxInfof(ctx, "BuyCommodity resp=%+v", resp)
	return
}

// GetOrderList implements the CommodityServiceImpl interface.
func (s *CommodityServiceImpl) GetOrderList(ctx context.Context, req *demo_commodity.GetOrderListReq) (resp *demo_commodity.GetOrderListResp, err error) {
	klog.CtxInfof(ctx, "GetOrderList req=%+v", req)
	resp = facade.OrderFacade.GetList(ctx, req)
	klog.CtxInfof(ctx, "GetOrderList resp=%+v", resp)
	return
}

// CommentOrder implements the CommodityServiceImpl interface.
func (s *CommodityServiceImpl) CommentOrder(ctx context.Context, req *demo_commodity.CommentOrderReq) (resp *demo_commodity.CommentOrderResp, err error) {
	klog.CtxInfof(ctx, "CommentOrder req=%+v", req)
	resp = facade.OrderFacade.Comment(ctx, req)
	klog.CtxInfof(ctx, "CommentOrder resp=%+v", resp)
	return
}
